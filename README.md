# kernel-install for Dracut

A package to enable systemd-boot automation on Arch-based distros using kernel-install with Dracut

The kernel-install hooks were adapted from the AUR package originally written by Tilmann Meyer

The package does a few things:
* Installs hooks to automate the installation and removal of kernels using kernel-install
* Enables support for both full fallback images and optimized "hostonly" images
* Saves kernel options to /etc/kernel/cmdline to support recovery in a chroot

There is a configuration file located in `/etc/kernel-install-for-dracut` with options described in the file.

To make changes to the kernel options/params, you can edit `/etc/kernel/cmdline` directly.  If you would like different kernel options for the fallback initrd, you can optionally create `/etc/kernel/cmdline_fb` with the same format.
